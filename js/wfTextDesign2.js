function addWfTextDesign2(id) {
    data["draggable"][id]["textContainer"] = data["draggable"][id]["group"].append("foreignObject")
        .data([{x: 0,
            y: 0,
            width: data["draggable"][id]["w"],
            height: data["draggable"][id]["h"]}])
        .attr("x", function (d) {return d.x;})
        .attr('y', function (d) {return d.y;})
        .attr('width', function (d) {return d.width;})
        .attr('height', function (d) {return d.height;});


    data["draggable"][id]["text"] = data["draggable"][id]["textContainer"]
        .append("xhtml:body")
        .attr('xmlns','http://www.w3.org/1999/xhtml')
        .append('textarea')
        .attr("id", "text")
        .attr("maxlength", 40)
        .property("disabled", true)
        .style("width", (data["draggable"][id]["w"]) + "px")
        .style("height", (data["draggable"][id]["h"])+ "px")
        .property("value", "double-click \n text \n to edit")
        .style("resize", "none")
        .style("background-color", "transparent")
        .style("overflow", "hidden")
        .style("border", 0)
        .style("font-size" , (parseInt(data["draggable"][id]["h"] / 4) - 2) + "px")
        .style("color", data["draggable"][id]["color"])
        .style("text-align", "center")
        //.property("position", "absolute")
        //.style("pointer-events", false)
        //.style("outline", "none")
        //.style("stroke", "black")
        //.style("stroke-width", 1)
        //.style("border", "0")
        //.style("font-family", "Arial")
        //.style("letter-spacing", ".3em")
        .on("mousedown", wfTextMousedownDesign2)
        .on("scroll", wfTextScrollDesign2)
        .on("focusout", wfTextFocusoutDesign2)
        //.on('keyup', function(){
        //    $(this).val($(this).val().replace(/[\r\n\v]+/g, ''));})
        .on("keydown", wfTextKeydownDesign2);

    adjustWfTextSizeDesign2(id);

};

function wfTextMousedownDesign2() {
    d3.event.stopPropagation();
};

function wfTextScrollDesign2() {
    console.log("scroll")
};

function wfTextFocusoutDesign2() {
    d3.select(this).property("disabled", true);
    d3.select(this).style("border", "0");
};

function wfTextKeydownDesign2() {
    console.log("Keydown: " + d3.event.keyCode)
    if (d3.event.keyCode == 27) {
        d3.select(this).property("disabled", true)
        d3.select(this).style("border", "0")}

    if (d3.event.keyCode == 13 && d3.select(this).property("value").split("\n").slice(-1) == ""){
        d3.select(this).property("disabled", true);
        d3.select(this).style("border", "0");
        // replace last \n
        d3.select(this).property("value", d3.select(this).property("value").replace(/\n$/, ""));
    }
};

function adjustWfTextSizeDesign2(id) {
    var w = data["draggable"][id]["w"];
    var h = data["draggable"][id]["h"];

    if (data["draggable"][id]["title"]) {
        //h = h - parseInt(data["draggable"][id]["title"].style("height"));
        h = parseInt((h / 3) * 2);
        var y = parseInt(data["draggable"][id]["title"].style("height"));
        data["draggable"][id]["text"].style("height", h + "px");
        data["draggable"][id]["text"].style("font-size", parseInt(parseInt(data["draggable"][id]["text"].style("height")) / 3 - 2 ) + "px");
        data["draggable"][id]["textContainer"].attr("y", y);
    };
    if (data["draggable"][id]["pic"]) {
        var x = parseInt(data["draggable"][id]["pic"].attr("height"));
        data["draggable"][id]["text"].style("width", (w - x - 10) + "px");
        data["draggable"][id]["textContainer"].attr("x", x + 10);
    };
}
