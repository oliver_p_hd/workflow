
function createRectShape() {
    var id = getRandomId();

    data["draggable"][id] = {};
    data["draggable"][id]["x"] = 100;
    data["draggable"][id]["y"] = 100;
    data["draggable"][id]["w"] = data["svg"]["w"] / 100 * data["xMulti"];
    data["draggable"][id]["h"] = data["draggable"][id]["w"] / 2;
    createWfGroup(id);
    addWfRect(id);
    addWfShadow(id);

    data["draggable"][id]["color"] = "#00FFFF";
    addWfColorGardient(id);
    addWfText(id);

    //data["draggable"][id]["mainText"].style("width", "50px")


};

function demoRect() {
    var id = getRandomId();

    data["draggable"][id] = {};
    data["draggable"][id]["x"] = getRandomInt(0, data.svg.w - 200);
    data["draggable"][id]["y"] = getRandomInt(0, data.svg.h - 100);
    console.log("x: " + data["draggable"][id]["x"])
    console.log(data.svg.w)
    console.log(data["draggable"][id]["y"])
    console.log(data.svg.h)
    data["draggable"][id]["w"] = parseInt(data["svg"]["w"] / 100 * data["xMulti"]);
    data["draggable"][id]["h"] = parseInt(data["draggable"][id]["w"] / 2);
    createWfGroup(id);
    addWfRect(id);
    addWfShadow(id);

    data["draggable"][id]["color"] = data.colors[getRandomInt(0, 19)];
    addWfColorGardient(id);
    if (getRandomInt(0,1)){addWfTitle(id);};
    if (getRandomInt(0,1)) {addWfPic(id);};
    addWfText(id);
    adjustWfTextSize(id);
    //data["draggable"][id]["mainText"].style("width", "50px")


};


function demoRectDesign2() {
    var id = getRandomId();

    data["draggable"][id] = {};
    data["draggable"][id]["x"] = getRandomInt(0, data.svg.w - 200);
    data["draggable"][id]["y"] = getRandomInt(0, data.svg.h - 100);

    data["draggable"][id]["w"] = parseInt(data["svg"]["w"] / 100 * data["xMulti"]);
    data["draggable"][id]["h"] = parseInt(data["draggable"][id]["w"] / 2);
    data["draggable"][id]["color"] = data.colors[getRandomInt(0, 19)];
    createWfGroup(id);
    addWfRectDesign2(id);
    //addWfShadow(id);
    addWfTextDesign2(id);
    if (getRandomInt(0,1)){addWfTitleDesign2(id);};

    adjustWfTextSize(id);
};

function connectDraggables(){
    if (data.connectActive){
        data.connectActive = false;
        d3.select("#button_connect").style("background-color", "")
    }
    else {
        data.connectActive = true;
        d3.select("#button_connect").style("background-color", "#FF7F50")
    };
    console.log(data.connectActive)
};

function switchBackground() {
    console.log(svg.style('background-color'))
    if (svg.style('background-color') == "rgb(244, 243, 241)") {
        svg.style('background-color', '#353533');
    } else {svg.style('background-color', '#f4f3f1');}
}
