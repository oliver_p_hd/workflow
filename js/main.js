var data = {};
    data["draggable"] = {};
    data["connectActive"] = false;
    data["connectionTmpId"] = null;
    data["connections"] = {};
    data["xMulti"] = 15;
    data["svg"] = {w: parseInt(document.documentElement.clientWidth),
                   h: parseInt(document.documentElement.clientHeight)};

    data["colors"] = {
        0: "#FFCBA4", 1: "#B6B6B4", 2: "#57E964", 3: "#E3E4FA",
        4: "#5CB3FF", 5: "#E0FFFF", 6: "#93FFE8", 7: "#FFCBA4",
        8: "00FFFF", 9: "#9AFEFF", 10: "#7FFFD4", 11: "#4EE2EC",
        12: "#99C68E", 13: "#64E986", 14: "#5EFB6E", 15: "#87F717",
        16: "#CCFB5D", 17: "#FFFF00", 18: "#FFFFC2", 19: "#FFDB58"}


var svg = d3.select("body").append('svg')
    .attr('preserveAspectRatio', 'xMinYMin meet')
    .attr('viewBox', '0 0 ' + data.svg.w + ' ' + (data.svg.h - document.getElementById("wfNavBar").offsetHeight - 10))
    .attr('width', '100%')
    .attr('height', '100%')
    .attr('style', 'outline: thin solid black;')
    .style('background-color', '#f4f3f1'); //353533


createRectShape();
for (i = 0; i < 5; i++) {
    demoRect();
    demoRectDesign2();
}

/*
var foreign = svg.append("foreignObject")
    .attr('x', 50)
    .attr('y', 20);

var textarea = foreign.append("xhtml:textarea")
    .attr("rows", 5)
    .attr("cols", 5)
    .style("background-color", "wheat")
    .html("This is a textarea inside a foreign object");

textarea.transition()
    .duration(3000)
    .attr("rows", 8)
    .attr("cols", 20)
*/



function wfBodyResize() {
    svg.attr('preserveAspectRatio', 'xMinYMin meet')
        .attr('viewBox', '0 0 ' + data.svg.w + ' ' + (data.svg.h - document.getElementById("wfNavBar").offsetHeight - 10));
    data["xMulti"] = 15;
    data["svg"] = {w: parseInt(document.documentElement.clientWidth),
        h: parseInt(document.documentElement.clientHeight)};
}
