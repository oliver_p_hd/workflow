function addWfConnection(id1, id2) {
    if(checkConnection(id1, id2)){
        createPath(id1, id2);
    };

};


function checkConnection(id1, id2) {
    if(!data.draggable[id1]["connectionTo"]){
        data.draggable[id1]["connectionTo"] = {};
    };
    if(!data.draggable[id2]["connectionFrom"]){
        data.draggable[id2]["connectionFrom"] = {};
    };
    if(data.draggable[id1]["connectionTo"][id2]) {
        console.log("Connection already exists!")
        return false;
    } else {
        data.draggable[id1]["connectionTo"][id2] = null;
        data.draggable[id2]["connectionFrom"][id1] = null;
    }
    return true;
};

function createPath(id1, id2) {
    var x1 = data.draggable[id1].x;
    var y1 = data.draggable[id1].y;
    var x2 = data.draggable[id2].x;
    var y2 = data.draggable[id2].y;

    var pathid = getRandomId();
    data.draggable[id1]["connectionTo"][id2] = pathid;
    data.draggable[id2]["connectionFrom"][id1] = pathid;


    var lineFunction = d3.line()
        .x(function(d) { return d.x; })
        .y(function(d) { return d.y; })
        .curve(d3.curveLinear);
        //.curve(d3.curveStepAfter);

    svg.append("defs").append("marker")
        .attr("id", "arrowhead")
        .attr("refX", 5)
        .attr("refY", 2)
        .attr("markerWidth", 6)
        .attr("markerHeight", 4)
        .attr("orient", "auto")
        .append("path")
        .attr("d", "M 0,0 V 4 L6,2 Z");


    var lineData = [
        { "x": x1,   "y": y1},
        { "x": x2,  "y": y2}];

    var lineGraph = svg.append("path")
        .attr("d", lineFunction(lineData))
        .attr("id", pathid)
        .attr("paint-order", "stroke fill;")
        .attr("stroke-width", 3)
        .style("stroke-dasharray", ("10,3"))
        .attr("marker-end", "url(#arrowhead)")
        .attr("stroke", "black")
        .style("fill", data.draggable[id1].color)
        .on("click", function () {
            console.log("click")
        });

    updateWfPath(id1)

}


function updateWfPath(id) {
    var lineFunction = d3.line()
        .x(function(d) { return d.x; })
        .y(function(d) { return d.y; })
        .curve(d3.curveLinear);

    for (var key in data.draggable[id].connectionTo) {
        if (data.draggable[id].connectionTo.hasOwnProperty(key)) {
            var x1 = data.draggable[id].x;
            var y1 = data.draggable[id].y;
            var w1 = data.draggable[id].w;
            var h1 = data.draggable[id].h;
            var x2 = data.draggable[key].x;
            var y2 = data.draggable[key].y;
            var w2 = data.draggable[key].w;
            var h2 = data.draggable[key].h;


            if((x1 + w1) < x2){
                x1 = x1 + w1 + 10;
                x2 = x2  - 10;
                y1 = y1 + h1 / 2;
                y2 = y2 + h2 / 2;
            } else if ((x2 + w2) < x1) {
                x1 = x1 - 10;
                x2 = x2 + w2 + 10;
                y1 = y1 + h1 / 2;
                y2 = y2 + h2 / 2;
            } else {
                x1 = x1 + w1 / 2;
                x2 = x2 + w2 / 2;

                if ((y1 + h1) < y2){
                    y1 = y1 + h1 + 10;
                    y2 = y2 -10;
                } else {
                    y1 = y1 - 10;
                    y2 = y2 + h2 + 10;
                }
            }

            d3.select("#" + data.draggable[id].connectionTo[key])
                .attr("d", lineFunction([
                    { "x": x1,   "y": y1},
                    { "x": x2,  "y": y2}]))

        }
    }
    for (var key in data.draggable[id].connectionFrom) {
        if (data.draggable[id].connectionFrom.hasOwnProperty(key)) {
            var x1 = data.draggable[key].x;
            var y1 = data.draggable[key].y;
            var w1 = data.draggable[key].w;
            var h1 = data.draggable[key].h;
            var x2 = data.draggable[id].x;
            var y2 = data.draggable[id].y;
            var w2 = data.draggable[id].w;
            var h2 = data.draggable[id].h;

            if((x1 + w1) < x2){
                x1 = x1 + w1 + 10;
                x2 = x2  - 10;
                y1 = y1 + h1 / 2;
                y2 = y2 + h2 / 2;
            } else if ((x2 + w2) < x1) {
                x1 = x1 - 10;
                x2 = x2 + w2 + 10;
                y1 = y1 + h1 / 2;
                y2 = y2 + h2 / 2;
            } else {
                x1 = x1 + w1 / 2;
                x2 = x2 + w2 / 2;

                if ((y1 + h1) < y2){
                    y1 = y1 + h1 + 10;
                    y2 = y2 -10;
                } else {
                    y1 = y1 - 10;
                    y2 = y2 + h2 + 10;
                }
            }

            d3.select("#" + data.draggable[id].connectionFrom[key])
                .attr("d", lineFunction([
                    { "x": x1,   "y": y1},
                    { "x": x2,  "y": y2}]))

        }
    }
};
