function addWfRectDesign2(id) {
    data["draggable"][id]["mainShape"] = data["draggable"][id]["group"].append("rect")
        .data([{x: 0,
            y: 0,
            width: data["draggable"][id]["w"],
            height: data["draggable"][id]["h"]}])
        .attr("id", "mainShape")
        .attr("x", function (d) {return d.x;})
        .attr('y', function (d) {return d.y;})
        .attr('width', function (d) {return d.width;})
        .attr('height', function (d) {return d.height;})
        .attr("rx", 6)
        .attr("ry", 6)
        .style("stroke", data["draggable"][id]["color"])
        //.style("stroke-opacity", 0.5)
        .style("stroke-width", 6)
        .style('opacity', 1)
        .attr("fill", "transparent")
        .attr("style", "outline: 4px solid " + data["draggable"][id]["color"] + ";")
    //.style("stroke-dasharray", ("10,3"))

};

