function addWfColorGardient(id) {
    //Append a defs (for definition) element to your SVG
    var defs = data["draggable"][id]["group"].append("defs");
    //Append a linearGradient element to the defs and give it a unique id
    this.gardient = defs.append("linearGradient")
        .attr("id", id + "linear-gradient")
    //.attr("gradientUnits", "userSpaceOnUse");
    //Horizontal gradient
    this.gardient
        .attr("x1", "-30%")
        .attr("y1", "-30%")
        .attr("x2", "120%")
        .attr("y2", "120%");
    //Set the color for the start (0%)
    this.gardient.append("stop")
        .attr("offset", "0%")
        .attr("stop-color", data["draggable"][id]["color"])
        .attr("stop-opacity", 1)
    //Set the color for the end (100%)
    this.gardient.append("stop")
        .attr("offset", "100%")
        .attr("stop-opacity", 0)
        .attr("stop-color", data["draggable"][id]["color"]);

    data["draggable"][id]["mainShape"].style("fill", "url(#" + id + "linear-gradient)");
};
