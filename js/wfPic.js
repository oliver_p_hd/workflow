function addWfPic (id) {
    var h = parseInt(data["draggable"][id]["h"] / 2);
    var w = h;
    var x = 5;
    var y = h - 5;


    data["draggable"][id]["pic"] = data["draggable"][id]["group"].append("image")
        .data([{x: x,
                y: y,
                width: w,
                height: h}])
        .attr("x", function (d) {return d.x;})
        .attr('y', function (d) {return d.y;})
        .attr('width', function (d) {return d.width;})
        .attr('height', function (d) {return d.height;})
        .attr("xlink:href","img/progress-report.png")
};
