function addWfTitleDesign2(id) {
    var x = data["draggable"][id]["x"];
    var y = data["draggable"][id]["y"];
    var w = data["draggable"][id]["w"];
    var h = parseInt(data["draggable"][id]["h"] / 3);

    data["draggable"][id]["titleContainer"] = data["draggable"][id]["group"].append("foreignObject")
        .data([{x: 0,
            y: 0,
            width: w,
            height: h}])
        .attr("x", function (d) {return d.x;})
        .attr('y', function (d) {return d.y;})
        .attr('width', function (d) {return d.width;})
        .attr('height', function (d) {return d.height;});

    data["draggable"][id]["title"] = data["draggable"][id]["titleContainer"]
        .append("xhtml:body")
        .attr('xmlns','http://www.w3.org/1999/xhtml')
        .append('input')
        .attr("id", "title-text")
        .property("value", "Add Title")
        .style("width", w + "px")
        .style("height", h + "px")
        .style("background-color", "transparent")
        .style("border", 0)
        .style("font-size" , h + "px")
        .style("color", data["draggable"][id]["color"])
        .style("overflow", "hidden")
        //.style("resize", "none")
        .property("disabled", true)
        .style("pointer-events", false)
        .style("outline", "none")
        .style("text-align", "center")
        .style("stroke", "black")
        .style("stroke-width", 1)
        .style("border", "0px")
        .style("font-family", "Arial")
        //.on("dblclick", function() {console.log("dbclick")})
        .on("focusout", function () {
            d3.select(this).property("disabled", true)
            d3.select(this).style("border", "0")
        })
        //.style("letter-spacing", ".3em")
        //.on("mousedown", function() { d3.event.stopPropagation();})
        .on("keypress", function() {
            if (d3.event.keyCode == 13 ){
                console.log("enter")
                d3.select(this).property("disabled", true)
                d3.select(this).style("border", "0")
            }
        })
        .on("keydown", function() {
            if (d3.event.keyCode == 27) {
                d3.select(this).property("disabled", true)
                d3.select(this).style("border", "0")
            }
        });
};
