

function createWfGroup(id) {
    data["draggable"][id]["group"] = svg.append("g")
        .datum({x: data["draggable"][id]["x"], y: data["draggable"][id]["y"]})
        .attr("class", "group")
        .attr("id", id)
        .call(d3.drag()
            .on("start", wfGroupDragStart)
            .on("drag", wfGroupDragged)
            .on("end", wfGroupDragEnd))
        .on("dblclick", wfGroupDbclicked)
        .on("click", wfGroupClicked)
        .attr("transform","translate(" + data["draggable"][id]["x"] + ", " + data["draggable"][id]["y"] +")");
};

function wfGroupDragStart(d) {
    console.log("start")
};

function wfGroupDragged(d) {
    var x = d3.event.x;
    var y = d3.event.y;
    d3.select(this).attr("transform", "translate(" + (d.x = x) + "," + (d.y = y) + ")");

    var id = d3.select(this).attr("id");
    data["draggable"][id]["x"] = parseInt(x);
    data["draggable"][id]["y"] = parseInt(y);
    updateWfPath(id);
};

function wfGroupDragEnd(d) {
}

function wfGroupDbclicked() {
    console.log("dbclicked");
    // check if textarea is added
    // check if mouse is over textarea and make it editable
    if (!d3.select(this).select("#text").empty()) {
        var id_name = "text";
        for (var i = 0; i < document.querySelectorAll( ":hover" ).length; i++) {
            if (document.querySelectorAll(":hover")[i].id == id_name) {
                d3.select(this).select("#" + id_name).property("disabled", false);
                d3.select(this).select("#" + id_name).style("border", "2px dotted");
            }
        }
    }
    // check if title-area is added
    // check if mouse is over title-area and make it editable
    if (!d3.select(this).select("#title-text").empty()) {
        var id_name = "title-text";
        for (var i = 0; i < document.querySelectorAll( ":hover" ).length; i++) {
            if (document.querySelectorAll(":hover")[i].id == id_name) {
                d3.select(this).select("#" + id_name).property("disabled", false);
                d3.select(this).select("#" + id_name).style("border", "2px dotted white");
            }
        }
    }
};

function wfGroupClicked() {
    var id = d3.select(this).attr("id");
    if (data.connectActive){
        if(!data.connectionTmpId){
            data.connectionTmpId = id;
        }
        if(data.connectionTmpId && !(data.connectionTmpId == id)){
            addWfConnection(data.connectionTmpId, id);
            data.connectionTmpId = null;
        }
    }
};
