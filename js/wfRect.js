function addWfRect(id) {
    data["draggable"][id]["mainShape"] = data["draggable"][id]["group"].append("rect")
        .data([{x: 0,
                y: 0,
                width: data["draggable"][id]["w"],
                height: data["draggable"][id]["h"]}])
        .attr("id", "mainShape")
        .attr("x", function (d) {return d.x;})
        .attr('y', function (d) {return d.y;})
        .attr('width', function (d) {return d.width;})
        .attr('height', function (d) {return d.height;})
        .attr("rx", 6)
        .attr("ry", 6)
        .style("stroke", "black")
        .style("stroke-opacity", 0.5)
        .style("stroke-width", 1)
        .style('opacity', 0.9)
        .attr("fill", "#6698FF")
    //.style("stroke-dasharray", ("10,3"))

};

